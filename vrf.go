//      _    _       ______      _______
//       \  /       |_____/      |______
//        \/        |    \_      |      
//                                      
//
// Copyright 2017 abhi shelat
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//        http://www.apache.org/licenses/LICENSE-2.0
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.



// Package vrf defines a generic interface for a Verifiable Random Function.
// 
// In the implemetnations that accompany this package, the idea is that a 
// PrivateKey object implements the VrfSK interface which allows evaluating the
// VRF and a PublicKey Object implements the Verifier interface for checking an
// evaluation.
//
// Both interfaces also require supporting a way to marshal and unmarshal the 
// objects from byte arrays.
//
// The first implementation is in sub-package "gitlab.com/abhvious/vrf/psvrf"
//
package vrf



type VrfSK interface {
	Vrf(msg []byte) ([]byte, []byte, error)
	GetPK() VrfPK
	Marshal() []byte
	Unmarshal([]byte) (VrfSK, error)
}


type VrfPK interface {
	Verify(msg, sig, proof []byte) bool
	Marshal() []byte
	Unmarshal([]byte) (VrfPK, error)
}


